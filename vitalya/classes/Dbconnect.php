<?php
//namespace Dbconnect;
class Dbconnect{
	const DBLOGIN = 'root';
	const DBPASS = 123;
	protected $connect;
	private $userId;

	public function __construct(){
		try{
			$this->connect = new PDO("mysql:host=localhost; dbname=vitalya", self::DBLOGIN, self::DBPASS, [
				PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
				//PDO::ATTR_PERSISTENT => true,
				]);
		}catch(PDOException $e){
			echo $e->getMessage();
			die();
		}
		
	}
// @if logged in then redirect if you have to return false
	final public function commandLogin($login, $password){
		if ((preg_match('/([\\/.`<>\*~!])+/', $login)) || (preg_match('/([\\/.`<>\*~!])+/', $password)))
		{
			return false;
		}
		$LoggedIn = $this->checkLogin($login, $password);
		if (!$LoggedIn)
		{
			return false;
		}
		$dataUser = $this->prepareUserAuthorized();
		if($dataUser)
		{
			header('location: index.php');
		}
		return true;
	}

	private function checkLogin($login, $password){
		
		$loggedIn = $this->connect->prepare("SELECT `id`, `password` FROM Login WHERE `login`= :login LIMIT 1");
		$loggedIn->bindParam(':login', $login);
		$loggedIn->execute();
		$arr= $loggedIn->fetchAll(PDO::FETCH_ASSOC);
				
		if (!(NULL != $arr) && !(1 == count($arr)))
		{
				return false;
		}
		if (password_verify($password, $arr[0]['password']))
		{
			$this->userId = $arr[0]['id'];
			return true;
		}
		
	}
	private function prepareUserAuthorized($connect){
		
		$dataUser = $this->connect->prepare("SELECT `id`, `NAME`,`avatar` FROM User WHERE `UserId` = :id LIMIT 1");
		$dataUser->bindParam(':id', $this->userId);
		$dataUser->execute();
		$arr = $dataUser->fetchAll(PDO::FETCH_ASSOC);
		
		if ((NULL != $arr) && (1 == count($arr)))
		{
			$_SESSION['auth'] = $arr[0]['NAME'];
			$_SESSION['avatar'] = $arr[0]['avatar'];
			$_SESSION['auth_id'] = $arr[0]['id'];
			return true;
		}
	}
}
