<?php

require_once('Dbconnect.php');

class UpdateUser extends DbConnect
{
	
	public function changeAvatar($avatar, $id)
	{
		//сделать проверку что это массив $avatar;
		if ((is_array($avatar)) && ($avatar != NULL))
		{
			if ($avatar['avatar']['error']  !== 0 )
			{
			    return false;
			}	
			if ($avatar['avatar']['size'] >16000)
			{
			    return false;	
			}
			$avatarType = basename($avatar['avatar']['type']);
			if (($avatarType != "jpeg") && ($avatarType !="jpg") && ($avatarType != "png") && ($avatarType != "gif"))
			{
			    return false;
			}
			$dir = 'img/';
			$targetFile = $dir . date('H:i:s') . $avatar['avatar']['name'];
			$file = $avatar['avatar']['tmp_name'];
					
			if (move_uploaded_file($file, $targetFile))
			{
				try
				{
					$this->connect->beginTransaction();
					$setImg = $this->connect->prepare("UPDATE User SET `avatar`= ? WHERE `UserId` = ?"); 
				    $setImg->execute([$targetFile, $id]);
				    $this->connect->commit();
					
				}
				catch(PDOException $e)
				{
				    $this->connect->rollBack();
				    echo $e->getMessage();
				}				
				return true;	
			}
			else
			{
			    return false;
			}
		}
	}
	public function ChengeName($newUserName, $userId)
	{
		try
		{
		    $this->connect->beginTransaction();	
		    $newN = $this->connect->quote($newUserName, PDO::PARAM_STR);
		    $uL = $this->connect->quote($userId, PDO::PARAM_STR);	
		    $this->connect->exec("UPDATE User SET `NAME`= $newN WHERE `UserId`= $uL");
		    $this->connect->commit();
		    return true;
		}
		catch (PDOException $e)
		{
		    $this->connect->rooBack();
		    echo $e->getMessage();
		}
	}
}
		
		
	

?>

			
