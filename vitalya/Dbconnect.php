<?php
class Dbconnect{
	const DBLOGIN = 'root';
	const DBPASS = 123;
	public $connect;
	public $user;

	public function __construct(){
		try{
			$this->connect = new PDO("mysql:host=localhost; dbname=vitalya", self::DBLOGIN, self::DBPASS, [
				PDO::ATTR_PERSISTENT => true,]);
		}catch(PDOException $e){
			echo $e->getMessage();
			die();
		}
	}
	public function checkLogin($login, $password){
		$test = $this->connect->prepare("SELECT * FROM User WHERE `login`= ? ");
		//$test->bindValue(':login', $login);
		//$test->bindValue(':password', $password);
		$test->execute([$login]);
		$arr = $test->fetchAll(PDO::FETCH_ASSOC);
		//var_dump($arr);
			if($arr != NULL){
				
				for($i=0; $i<count($arr); $i++){
					//echo $i . "<br>";
					if($arr[$i]['login'] && password_verify($password, $arr[$i]['password'])){
						//echo $arr[$i]['login'] . "<br>";
						$_SESSION['auth'] = TRUE;
						if($arr[$i]['access'] == 'admin'){
							 header('location:controlPanel.php');
							//echo 'Вы Админ';
						}
						if($arr[$i]['access'] == 'user'){	
							header('location:index.php');
							//echo 'вы юзер';
						}
						
					}//else echo 'Неверный пароль или логин1';
				}
			}else echo 'Не верный логин или пароль!';
	}
}
